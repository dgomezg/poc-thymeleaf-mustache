package com.tnt.ing.tpa.poc.templating;

import java.util.Map;

public interface TemplateRenderer {
    String render(String templateName, Map<String, Object> values);
}
