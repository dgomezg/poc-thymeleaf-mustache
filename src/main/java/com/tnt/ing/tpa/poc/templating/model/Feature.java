package com.tnt.ing.tpa.poc.templating.model;

public class Feature {
    private final String description;

    public String getDescription() {
        return description;
    }

    public Feature(String description) {

        this.description = description;
    }
}
