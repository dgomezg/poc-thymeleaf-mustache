package com.tnt.ing.tpa.poc.templating.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class Item {
    private final String name;
    private final BigDecimal price;
    private final LocalDate createdAt;
    private List<Feature> features;

    public Item(String name, BigDecimal price) {
        this(name, price, LocalDate.now());
    }

    public Item(String name, BigDecimal price, LocalDate createdAt, Feature ... features) {
        this.name = name;
        this.price = price;
        this.createdAt = createdAt;
        this.features = Arrays.asList(features);
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public List<Feature> getFeatures() {
        return features;
    }
}
