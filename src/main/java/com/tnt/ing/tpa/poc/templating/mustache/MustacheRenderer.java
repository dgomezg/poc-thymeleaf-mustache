package com.tnt.ing.tpa.poc.templating.mustache;

import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.tnt.ing.tpa.poc.templating.TemplateRenderer;

import java.io.StringWriter;
import java.util.Map;

public class MustacheRenderer implements TemplateRenderer {

    private final MustacheFactory mustacheFactory;

    public MustacheRenderer(MustacheFactory mustacheFactory) {
        this.mustacheFactory = mustacheFactory;
    }

    public String render(String templateName, Map<String, Object> values) {
        try {
            Mustache mustache = mustacheFactory.compile(templateName);
            StringWriter writer = new StringWriter();
            mustache.execute(writer, values);
            return writer.toString();
        } catch (Exception ex) {
            throw new RuntimeException("Could not render message using template", ex);
        }
    }

}
