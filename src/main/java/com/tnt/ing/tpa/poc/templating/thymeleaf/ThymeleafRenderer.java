package com.tnt.ing.tpa.poc.templating.thymeleaf;

import com.tnt.ing.tpa.poc.templating.TemplateRenderer;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.util.Locale;
import java.util.Map;

public class ThymeleafRenderer implements TemplateRenderer {

    private final ITemplateEngine templateEngine;

    public ThymeleafRenderer(ITemplateResolver templateResolver) {
        TemplateEngine engine = new TemplateEngine();
        engine.setTemplateResolver(templateResolver);
        this.templateEngine = engine;
    }

    @Override
    public String render(String templateName, Map<String, Object> values) {
        Context context = new Context(Locale.getDefault(), values);
        return templateEngine.process(templateName, context);
    }

}
