package com.tnt.ing.tpa.poc.templating;

import com.tnt.ing.tpa.poc.templating.model.Feature;
import com.tnt.ing.tpa.poc.templating.model.Item;
import com.tnt.ing.tpa.poc.templating.mustache.MustacheRendererTest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SampleDataFactory {

    public static Map<String, Object> createSampleData() {
        Map<String, Object> context = new HashMap<String, Object>();

        context.put("name", "John Doe");
        context.put("date", LocalDateTime.now());
        context.put("items", Arrays.asList(
                new Item("Item 1", BigDecimal.valueOf(99.9), LocalDate.of(2017, Month.JANUARY, 1), new Feature("New")),
                new Item("Item 2", BigDecimal.valueOf(10_230.9), LocalDate.of(2016, Month.JANUARY, 1), new Feature("Old"), new Feature("Big"))
        ));

        return context;

    }
}