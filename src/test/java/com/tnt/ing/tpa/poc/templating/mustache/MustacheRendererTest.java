package com.tnt.ing.tpa.poc.templating.mustache;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.ObjectHandler;
import com.github.mustachejava.reflect.ReflectionObjectHandler;
import com.github.mustachejava.util.Wrapper;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.tnt.ing.tpa.poc.templating.SampleDataFactory.createSampleData;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

public class MustacheRendererTest {

    private static final String BASE_PATH = "tpl/mustache";

    private static DefaultMustacheFactory mustacheFactory = new DefaultMustacheFactory(MustacheRendererTest::resolveTemplate);

    private MustacheRenderer renderer = new MustacheRenderer(mustacheFactory);

    @Test
    public void shouldRenderASimpleTemplate() {
        String rendered = renderer.render("simple.tpl", createSampleData());
        assertThat(rendered, containsString("Name: Item 1\nPrice: 99.9\n   Feature: New"));
        assertThat(rendered, containsString("Name: Item 2\nPrice: 10230.9\n   Feature: Old\n   Feature: Big"));
    }

    @Test
    public void shouldRenderComposableTemplates() {
        Map<String, Object> context = createSampleData();
        context.put("renderer", renderer.getClass().getSimpleName());

        String rendered = renderer.render("container.tpl", context);

        assertThat(rendered, containsString("the following Items can be found")); //Rendered by container.tpl
        assertThat(rendered, containsString("Name: Item 1 - Price: 99.9"));          //rendered by included intems-list.tpl
        assertThat(rendered, containsString("Name: Item 2 - Price: 10230.9"));      //rendered by included intems-list.tpl
        assertThat(rendered, containsString("Rendered by " + renderer.getClass().getSimpleName()));         //rendered by footer.tpl
    }

    @Test
    public void shouldRenderOptionalValueWhenPresent() {
        Map<String,Object> context = new HashMap<>();
        context.put("value", "present");

        String rendered = renderer.render("optionalValues.tpl", context);

        assertThat(rendered, containsString("Value is present"));
    }

    @Test
    public void shouldRenderDefaultValueWhenOptionaValueIsNotPresent() {
        Map<String,Object> emptyContext = new HashMap<>();
        String rendered = renderer.render("optionalValues.tpl", emptyContext);
        assertThat(rendered, containsString("Value is default value"));
    }

    @Test
    public void shouldRenderOptionalValuesWithElvisOperator() {
        mustacheFactory.setObjectHandler(createElvisOperatorObjectHandler());
        Map<String,Object> context = new HashMap<>();

        context.put("value", "present");
        String rendered = renderer.render("optionalValues-withCustomParser.tpl", context);
        assertThat(rendered, containsString("Value is present"));

        context.remove("value");
        rendered = renderer.render("optionalValues-withCustomParser.tpl", context);
        assertThat(rendered, containsString("Value is default value"));
    }


    @Test
    public void shouldRenderAValueTuncatedWithEllipsisWhenMaximunLenghtSpecified() {
        //Register Specific ObjectHandler which truncates with ellipsis (...)
        mustacheFactory.setObjectHandler(createMaxLengthObjectHandler());

        Map<String,Object> context = new HashMap<>();

        context.put("name", "David"); //A name shorter than max length
        String rendered = renderer.render("values-with-maximun-length.tpl", context);
        assertThat(rendered, containsString("Hello David"));

        context.put("name", "Maximilian"); //A name with just 10 characters long (exactly the limit)
        rendered = renderer.render("values-with-maximun-length.tpl", context);
        assertThat(rendered, containsString("Hello Maximilian"));

        context.put("name", "Shakespeare"); //A name with just 10 characters long (exactly the limit)
        rendered = renderer.render("values-with-maximun-length.tpl", context);
        assertThat(rendered, containsString("Hello Shakesp..."));
    }
    @Test
    public void shouldRenderDatesUsingSpecifiedFormat() {
        mustacheFactory.setObjectHandler(createDateFormatingObjectHandler());
        Map<String,Object> context = new HashMap<>();
        context.put("date", LocalDateTime.of(2017, Month.JANUARY, 13, 10, 17));

        String rendered = renderer.render("date.tpl", context);

        assertThat(rendered, containsString("Given date is 13/01/2017 10:17"));
    }

    private ObjectHandler createElvisOperatorObjectHandler() {
        return new ReflectionObjectHandler() {
            @Override
            public Wrapper find(String name, List<Object> scopes) {
                int separatorPos = name.indexOf("?:");
                if (separatorPos > 0) { // date?:date-format
                    String fieldName = name.substring(0, separatorPos);
                    String defaultExpression = name.substring(separatorPos + 2, name.length());
                    final Wrapper wrapper = super.find(fieldName.trim(), scopes);
                    return createMaxLengthWrapperFor(wrapper, defaultExpression.trim());
                } else {
                    return super.find(name, scopes);
                }
            }

            private Wrapper createMaxLengthWrapperFor(Wrapper wrapper, String defaultExpression) {
                return scopes -> { //call method on Wrapper
                    final Object value = wrapper.call(scopes);
                    if (value != null) {
                        return value;
                    } else {
                        if (defaultExpression.startsWith("'")) {
                            return defaultExpression.substring(1, defaultExpression.lastIndexOf("'"));
                        } else {
                            return super.find(defaultExpression, scopes);
                        }
                    }
                };
            }
        };
    }

    private ObjectHandler createDateFormatingObjectHandler() {
        return new ReflectionObjectHandler() {
            @Override
            public Wrapper find(String name, List<Object> scopes) {
                int separatorPos = name.indexOf("#");
                if (separatorPos > 0) { // date:date-format
                    String fieldName = name.substring(0, separatorPos);
                    String length = name.substring(separatorPos +1, name.length());
                    final Wrapper wrapper = super.find(fieldName.trim(), scopes);
                    return createMaxLengthWrapperFor(wrapper, length);
                } else {
                    return super.find(name, scopes);
                }
            }

            private Wrapper createMaxLengthWrapperFor(Wrapper wrapper, String pattern) {
                return scopes -> { //call method on Wrapper
                    final Object value = wrapper.call(scopes);
                    if (value != null && value instanceof LocalDateTime) {
                        LocalDateTime localDate = (LocalDateTime)value;
                        return localDate.format(DateTimeFormatter.ofPattern(pattern));
                    } else {
                        return value;
                    }
                };
            }
        };
    }



    private ObjectHandler createMaxLengthObjectHandler() {
        return new ReflectionObjectHandler() {
            @Override
            public Wrapper find(String name, List<Object> scopes) {
                int separatorPos = name.indexOf(":");
                if (separatorPos > 0) { // name:maxlength format
                    String fieldName = name.substring(0, separatorPos);
                    int length = Integer.valueOf(name.substring(separatorPos +1, name.length()));
                    final Wrapper wrapper = super.find(fieldName.trim(), scopes);
                    return createMaxLengthWrapperFor(wrapper, length);
                } else {
                    return super.find(name, scopes);
                }
            }

            private Wrapper createMaxLengthWrapperFor(Wrapper wrapper, int length) {
                return scopes -> { //call method on Wrapper
                        final Object value = wrapper.call(scopes);
                        if (value != null && value instanceof String) {
                            String stringValue = (String)value;
                            return stringValue.length()<=length? stringValue : stringValue.substring(0, length-3) + "...";
                        } else {
                            return value;
                        }
                    };
            }
        };
    }

    private static Reader resolveTemplate(String templateName) {
        Path templateFilePath = Paths.get(BASE_PATH, templateName);
        try {
            return new FileReader(ClassLoader.getSystemResource(templateFilePath.toString()).getPath());
        } catch (FileNotFoundException fnfEx) {
            return null;
        }
    }

}
