package com.tnt.ing.tpa.poc.templating.thymeleaf;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.DefaultTemplateResolver;
import org.thymeleaf.templateresolver.FileTemplateResolver;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.tnt.ing.tpa.poc.templating.SampleDataFactory.createSampleData;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

public class ThymeleafRendererTest {

    private static final String BASE_PATH = "tpl/thymeleaf/";

    private ThymeleafRenderer renderer;

    @Before
    public void setUp() {
        FileTemplateResolver templateResolver = new FileTemplateResolver();
        templateResolver.setTemplateMode(TemplateMode.TEXT);
        templateResolver.setPrefix(ClassLoader.getSystemResource(BASE_PATH).getPath().toString());

        renderer = new ThymeleafRenderer(templateResolver);
    }

    @Test
    public void shouldRenderASimpleTemplate() {
        String rendered = renderer.render("simple.tpl", createSampleData());
        assertThat(rendered, containsString("Name is John Doe"));
    }

    @Test
    public void shouldRenderComposableTemplates() {
        Map<String, Object> context = createSampleData();
        context.put("renderer", renderer.getClass().getSimpleName());

        String rendered = renderer.render("container.tpl", context);

        assertThat(rendered, containsString("the following Items can be found"));    //Rendered by container.tpl
        assertThat(rendered, containsString("Name: Item 1 - Price: 99.9"));          //rendered by included intems-list.tpl
        assertThat(rendered, containsString("Name: Item 2 - Price: 10230.9"));       //rendered by included intems-list.tpl
        assertThat(rendered, containsString("Rendered by " + renderer.getClass().getSimpleName()));         //rendered by footer.tpl
    }

    @Test
    public void shouldRenderOptionalValueWhenPresent() {
        Map<String, Object> context = new HashMap<>();
        context.put("value", "present");

        String rendered = renderer.render("optionalValues.tpl", context);
        assertThat(rendered, containsString("Value is present"));
    }

    @Test
    public void shouldRenderDefaultValueWhenOptionaValueIsNotPresent() {
        Map<String, Object> context = new HashMap<>();

        String rendered = renderer.render("optionalValues.tpl", context);
        assertThat(rendered, containsString("Value is default value"));
    }

    @Test
    public void shouldRenderAStringWithASpecifiedMaximunLenghtWithEllipsisAtThenWhenLongerValueProvided() {
        Map<String, Object> context = new HashMap<>();
        context.put("name", "David"); //A short name

        String rendered = renderer.render("values-with-maximun-length.tpl", context);
        assertThat(rendered, containsString("Hello David"));

        context.put("name", "Maximilian"); //A name with just 10 characters long (exactly the limit)
        rendered = renderer.render("values-with-maximun-length.tpl", context);
        assertThat(rendered, containsString("Hello Maximilian"));

        context.put("name", "Shakespeare"); //A name with just 10 characters long (exactly the limit)
        rendered = renderer.render("values-with-maximun-length.tpl", context);
        assertThat(rendered, containsString("Hello Shakesp..."));
    }

    @Test
    public void shouldRenderDatesUsingSpecifiedFormat() {
        Map<String, Object> context = new HashMap<>();
        //Unfortunately, Thymeleaf has not support for JSR310 objects, so we will need to convert them to Dates.
        context.put("date", Date.from(LocalDateTime.of(2017, Month.JANUARY, 12, 17,20).atZone(ZoneId.systemDefault()).toInstant()));

        String rendered = renderer.render("date.tpl", context);
        assertThat(rendered, containsString("Given date is 12/01/2017 17:20"));
    }


}
